#include "debug.h"
#include <stdio.h>
#include <string.h>

void USART1_Init(void);
void USART1_SET_IN_FLOATING(void);
void USART1_Send();
char USART1_Read();
void USART1_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));

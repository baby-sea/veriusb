#include "debug.h"
#include "oled.h"
#include "usart.h"
#include "stdlib.h"
#include <stdio.h>
#include <string.h>

int main(void)
{
    u8 i=0;
    u8 sign=0,length=0;
    u8 b[6]={0};
    u8 signal_yes[10]="COM";
    u8 signal_no[7]="Nothing";

    u8 size =16 ;//字符大小

    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
    SystemCoreClockUpdate();
    Delay_Init();
    USART1_Init();
    OLED_Init();

    while(sign==0&&i<100)
    {
        i++;
        sign=USART1_Read();
        Delay_Ms(10);
    }
    itoa(sign,b,10);
    strcat(signal_yes,b);
    length=strlen(signal_yes);
    if(sign!=0)
    {
        OLED_ColorTurn(0);//0正常显示，1 反色显示
        OLED_DisplayTurn(0);//0正常显示 1 屏幕翻转显示
        OLED_DrawLine(0,4,59,4,1);
        OLED_DrawLine(0,27,59,27,1);
        OLED_ShowString(30-size/4*length,8,signal_yes,size,1);
        OLED_Refresh();
    }
    else
    {
        OLED_ColorTurn(0);//0正常显示，1 反色显示
        OLED_DisplayTurn(0);//0正常显示 1 屏幕翻转显示
        OLED_DrawLine(0,4,59,4,1);
        OLED_DrawLine(0,27,59,27,1);
        OLED_ShowString(30-size/4*7,8,signal_no,size,1);
        OLED_Refresh();
    }
    USART_Cmd(USART1, DISABLE);
    USART1_SET_IN_FLOATING();
    while(1)
    {
        Delay_Ms(100);
    }
}

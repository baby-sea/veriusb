目录结构

    根目录
      ├─doc         说明文档目录
      ├─firmware    固件目录，存放与PCB使用相关的固件代码
      ├─machine     机械零件目录
      ├─mfdoc       生产PCB使用的文件，包括Gerber,BOM等
      ├─PCB         PCB工程
      │  ├─gerber   输出的Gerber文件
      │  └─other    设计中用到的资料(如datasheet)
      └─software    上位机软件或脚本

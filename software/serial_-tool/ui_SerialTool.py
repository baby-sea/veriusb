# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'SerialTool.ui'
##
## Created by: Qt User Interface Compiler version 6.7.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QComboBox, QFrame,
    QGraphicsView, QGridLayout, QHBoxLayout, QLabel,
    QMainWindow, QMenuBar, QPlainTextEdit, QPushButton,
    QSizePolicy, QStatusBar, QTabWidget, QTextBrowser,
    QVBoxLayout, QWidget)
import resources_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(970, 691)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout_3 = QHBoxLayout(self.centralwidget)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.gridLayout_5 = QGridLayout()
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.plainTextEdit_send = QPlainTextEdit(self.centralwidget)
        self.plainTextEdit_send.setObjectName(u"plainTextEdit_send")

        self.gridLayout_5.addWidget(self.plainTextEdit_send, 1, 1, 1, 1)

        self.Button_send = QPushButton(self.centralwidget)
        self.Button_send.setObjectName(u"Button_send")
        font = QFont()
        font.setPointSize(12)
        self.Button_send.setFont(font)

        self.gridLayout_5.addWidget(self.Button_send, 1, 0, 1, 1)

        self.tabWidget = QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tabWidget.setFont(font)
        self.tabWidget.setTabShape(QTabWidget.TabShape.Triangular)
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.horizontalLayout_5 = QHBoxLayout(self.tab)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.textBrowser_rec = QTextBrowser(self.tab)
        self.textBrowser_rec.setObjectName(u"textBrowser_rec")

        self.horizontalLayout_5.addWidget(self.textBrowser_rec)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.horizontalLayout_4 = QHBoxLayout(self.tab_2)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.graphicsView = QGraphicsView(self.tab_2)
        self.graphicsView.setObjectName(u"graphicsView")

        self.horizontalLayout_4.addWidget(self.graphicsView)

        self.tabWidget.addTab(self.tab_2, "")

        self.gridLayout_5.addWidget(self.tabWidget, 0, 1, 1, 1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.setlayout = QFrame(self.centralwidget)
        self.setlayout.setObjectName(u"setlayout")
        self.setlayout.setMaximumSize(QSize(240, 250))
        self.verticalLayout = QVBoxLayout(self.setlayout)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label_4 = QLabel(self.setlayout)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setFont(font)

        self.gridLayout_2.addWidget(self.label_4, 2, 0, 1, 1)

        self.label_3 = QLabel(self.setlayout)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setFont(font)

        self.gridLayout_2.addWidget(self.label_3, 3, 0, 1, 1)

        self.label_2 = QLabel(self.setlayout)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font)

        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)

        self.label_5 = QLabel(self.setlayout)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font)

        self.gridLayout_2.addWidget(self.label_5, 4, 0, 1, 1)

        self.label = QLabel(self.setlayout)
        self.label.setObjectName(u"label")
        self.label.setMinimumSize(QSize(8, 0))
        self.label.setMaximumSize(QSize(50, 16777215))
        self.label.setFont(font)

        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)

        self.comboBox_stopbit = QComboBox(self.setlayout)
        self.comboBox_stopbit.addItem("")
        self.comboBox_stopbit.addItem("")
        self.comboBox_stopbit.addItem("")
        self.comboBox_stopbit.setObjectName(u"comboBox_stopbit")
        self.comboBox_stopbit.setMinimumSize(QSize(0, 0))
        self.comboBox_stopbit.setMaximumSize(QSize(150, 16777215))
        self.comboBox_stopbit.setFont(font)

        self.gridLayout_2.addWidget(self.comboBox_stopbit, 4, 1, 1, 1)

        self.comboBox_parity = QComboBox(self.setlayout)
        self.comboBox_parity.addItem("")
        self.comboBox_parity.addItem("")
        self.comboBox_parity.addItem("")
        self.comboBox_parity.addItem("")
        self.comboBox_parity.setObjectName(u"comboBox_parity")
        self.comboBox_parity.setMinimumSize(QSize(0, 0))
        self.comboBox_parity.setMaximumSize(QSize(150, 16777215))
        self.comboBox_parity.setFont(font)

        self.gridLayout_2.addWidget(self.comboBox_parity, 3, 1, 1, 1)

        self.comboBox_datebit = QComboBox(self.setlayout)
        self.comboBox_datebit.addItem("")
        self.comboBox_datebit.addItem("")
        self.comboBox_datebit.addItem("")
        self.comboBox_datebit.addItem("")
        self.comboBox_datebit.setObjectName(u"comboBox_datebit")
        self.comboBox_datebit.setMinimumSize(QSize(0, 0))
        self.comboBox_datebit.setMaximumSize(QSize(150, 16777215))
        self.comboBox_datebit.setFont(font)

        self.gridLayout_2.addWidget(self.comboBox_datebit, 2, 1, 1, 1)

        self.comboBox_baud = QComboBox(self.setlayout)
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.addItem("")
        self.comboBox_baud.setObjectName(u"comboBox_baud")
        self.comboBox_baud.setMaximumSize(QSize(150, 16777215))
        self.comboBox_baud.setFont(font)

        self.gridLayout_2.addWidget(self.comboBox_baud, 1, 1, 1, 1)

        self.horizontalFrame = QFrame(self.setlayout)
        self.horizontalFrame.setObjectName(u"horizontalFrame")
        sizePolicy = QSizePolicy(QSizePolicy.Policy.Preferred, QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.horizontalFrame.sizePolicy().hasHeightForWidth())
        self.horizontalFrame.setSizePolicy(sizePolicy)
        self.horizontalFrame.setMinimumSize(QSize(0, 0))
        self.horizontalFrame.setMaximumSize(QSize(160, 16777215))
        self.horizontalLayout = QHBoxLayout(self.horizontalFrame)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.Button_refresh = QPushButton(self.horizontalFrame)
        self.Button_refresh.setObjectName(u"Button_refresh")
        sizePolicy1 = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.Button_refresh.sizePolicy().hasHeightForWidth())
        self.Button_refresh.setSizePolicy(sizePolicy1)
        self.Button_refresh.setMinimumSize(QSize(0, 0))
        self.Button_refresh.setMaximumSize(QSize(30, 16777215))
        self.Button_refresh.setFont(font)
        self.Button_refresh.setMouseTracking(True)
        self.Button_refresh.setStyleSheet(u"background-image: url(:/icons/images/icons/cil-loop-circular.png);\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"border: none;\n"
"border-left: 5px solid transparent;\n"
"background-color: rgb(113, 126, 149);")

        self.horizontalLayout.addWidget(self.Button_refresh)

        self.comboBox_port = QComboBox(self.horizontalFrame)
        self.comboBox_port.setObjectName(u"comboBox_port")
        self.comboBox_port.setMaximumSize(QSize(100, 16777215))
        self.comboBox_port.setFont(font)

        self.horizontalLayout.addWidget(self.comboBox_port)

        self.horizontalLayout.setStretch(0, 1)
        self.horizontalLayout.setStretch(1, 6)

        self.gridLayout_2.addWidget(self.horizontalFrame, 0, 1, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout_2)

        self.Button_connect = QPushButton(self.setlayout)
        self.Button_connect.setObjectName(u"Button_connect")
        self.Button_connect.setMinimumSize(QSize(0, 0))
        self.Button_connect.setMaximumSize(QSize(210, 16777215))
        self.Button_connect.setFont(font)

        self.verticalLayout.addWidget(self.Button_connect)


        self.verticalLayout_2.addWidget(self.setlayout)

        self.line = QFrame(self.centralwidget)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.Shape.HLine)
        self.line.setFrameShadow(QFrame.Shadow.Sunken)

        self.verticalLayout_2.addWidget(self.line)

        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.checkBox_recHex = QCheckBox(self.centralwidget)
        self.checkBox_recHex.setObjectName(u"checkBox_recHex")
        self.checkBox_recHex.setFont(font)

        self.gridLayout_3.addWidget(self.checkBox_recHex, 1, 0, 1, 1)

        self.label_6 = QLabel(self.centralwidget)
        self.label_6.setObjectName(u"label_6")
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)
        self.label_6.setMinimumSize(QSize(0, 0))
        self.label_6.setFont(font)

        self.gridLayout_3.addWidget(self.label_6, 0, 0, 1, 1)

        self.Button_clear = QPushButton(self.centralwidget)
        self.Button_clear.setObjectName(u"Button_clear")
        self.Button_clear.setFont(font)

        self.gridLayout_3.addWidget(self.Button_clear, 1, 1, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_3)

        self.line_3 = QFrame(self.centralwidget)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.Shape.HLine)
        self.line_3.setFrameShadow(QFrame.Shadow.Sunken)

        self.verticalLayout_2.addWidget(self.line_3)

        self.gridLayout_4 = QGridLayout()
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.label_7 = QLabel(self.centralwidget)
        self.label_7.setObjectName(u"label_7")
        sizePolicy.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy)
        self.label_7.setFont(font)

        self.gridLayout_4.addWidget(self.label_7, 1, 0, 1, 1)

        self.checkBox_sendHex = QCheckBox(self.centralwidget)
        self.checkBox_sendHex.setObjectName(u"checkBox_sendHex")
        self.checkBox_sendHex.setFont(font)

        self.gridLayout_4.addWidget(self.checkBox_sendHex, 2, 0, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_4)

        self.verticalLayout_2.setStretch(0, 10)
        self.verticalLayout_2.setStretch(1, 1)
        self.verticalLayout_2.setStretch(2, 3)
        self.verticalLayout_2.setStretch(3, 1)
        self.verticalLayout_2.setStretch(4, 3)

        self.horizontalLayout_2.addLayout(self.verticalLayout_2)

        self.line_2 = QFrame(self.centralwidget)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.Shape.VLine)
        self.line_2.setFrameShadow(QFrame.Shadow.Sunken)

        self.horizontalLayout_2.addWidget(self.line_2)

        self.horizontalLayout_2.setStretch(0, 10)

        self.gridLayout_5.addLayout(self.horizontalLayout_2, 0, 0, 1, 1)

        self.gridLayout_5.setRowStretch(0, 10)
        self.gridLayout_5.setRowStretch(1, 1)
        self.gridLayout_5.setColumnStretch(0, 4)
        self.gridLayout_5.setColumnStretch(1, 12)

        self.horizontalLayout_3.addLayout(self.gridLayout_5)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 970, 33))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.Button_send.setText(QCoreApplication.translate("MainWindow", u"\u53d1\u9001", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("MainWindow", u"\u63a5\u6536\u7a97\u53e3", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("MainWindow", u"\u4e32\u53e3\u7ed8\u56fe", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"\u6570\u636e\u4f4d", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"\u6821\u9a8c\u4f4d", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"\u6ce2\u7279\u7387", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"\u505c\u6b62\u4f4d", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"\u7aef\u53e3\u540d", None))
        self.comboBox_stopbit.setItemText(0, QCoreApplication.translate("MainWindow", u"1", None))
        self.comboBox_stopbit.setItemText(1, QCoreApplication.translate("MainWindow", u"1.5", None))
        self.comboBox_stopbit.setItemText(2, QCoreApplication.translate("MainWindow", u"2", None))

        self.comboBox_parity.setItemText(0, QCoreApplication.translate("MainWindow", u"None", None))
        self.comboBox_parity.setItemText(1, QCoreApplication.translate("MainWindow", u"Even", None))
        self.comboBox_parity.setItemText(2, QCoreApplication.translate("MainWindow", u"Mark", None))
        self.comboBox_parity.setItemText(3, QCoreApplication.translate("MainWindow", u"Odd", None))

        self.comboBox_datebit.setItemText(0, QCoreApplication.translate("MainWindow", u"8", None))
        self.comboBox_datebit.setItemText(1, QCoreApplication.translate("MainWindow", u"7", None))
        self.comboBox_datebit.setItemText(2, QCoreApplication.translate("MainWindow", u"6", None))
        self.comboBox_datebit.setItemText(3, QCoreApplication.translate("MainWindow", u"5", None))

        self.comboBox_baud.setItemText(0, QCoreApplication.translate("MainWindow", u"300", None))
        self.comboBox_baud.setItemText(1, QCoreApplication.translate("MainWindow", u"600", None))
        self.comboBox_baud.setItemText(2, QCoreApplication.translate("MainWindow", u"1200", None))
        self.comboBox_baud.setItemText(3, QCoreApplication.translate("MainWindow", u"2400", None))
        self.comboBox_baud.setItemText(4, QCoreApplication.translate("MainWindow", u"4800", None))
        self.comboBox_baud.setItemText(5, QCoreApplication.translate("MainWindow", u"9600", None))
        self.comboBox_baud.setItemText(6, QCoreApplication.translate("MainWindow", u"14400", None))
        self.comboBox_baud.setItemText(7, QCoreApplication.translate("MainWindow", u"19200", None))
        self.comboBox_baud.setItemText(8, QCoreApplication.translate("MainWindow", u"38400", None))
        self.comboBox_baud.setItemText(9, QCoreApplication.translate("MainWindow", u"56000", None))
        self.comboBox_baud.setItemText(10, QCoreApplication.translate("MainWindow", u"57600", None))
        self.comboBox_baud.setItemText(11, QCoreApplication.translate("MainWindow", u"115200", None))
        self.comboBox_baud.setItemText(12, QCoreApplication.translate("MainWindow", u"128000", None))
        self.comboBox_baud.setItemText(13, QCoreApplication.translate("MainWindow", u"256000", None))
        self.comboBox_baud.setItemText(14, QCoreApplication.translate("MainWindow", u"460800", None))
        self.comboBox_baud.setItemText(15, QCoreApplication.translate("MainWindow", u"512000", None))
        self.comboBox_baud.setItemText(16, QCoreApplication.translate("MainWindow", u"750000", None))
        self.comboBox_baud.setItemText(17, QCoreApplication.translate("MainWindow", u"921600", None))
        self.comboBox_baud.setItemText(18, QCoreApplication.translate("MainWindow", u"1500000", None))

        self.Button_refresh.setText("")
        self.Button_connect.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00", None))
        self.checkBox_recHex.setText(QCoreApplication.translate("MainWindow", u"\u5341\u516d\u8fdb\u5236", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"\u63a5\u6536\u8bbe\u7f6e", None))
        self.Button_clear.setText(QCoreApplication.translate("MainWindow", u"\u6e05\u7a7a\u6570\u636e", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"\u53d1\u9001\u8bbe\u7f6e", None))
        self.checkBox_sendHex.setText(QCoreApplication.translate("MainWindow", u"\u5341\u516d\u8fdb\u5236", None))
    # retranslateUi

